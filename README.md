**Readme**

First of all, thanks for this oportunity!

- This test was made using IDE Eclipse with pure java technology, may be, with more little bit time it could be implemented using Mulesoft IDE.

- I used CXF for SOAP Services and grizzly for Rest Services
- For make it work just run the main method in the br.com.avenue.Main class and will be possible to see both services running as follow log message is showing:

Rest Services: in br.com.avenue.soap.services.EmployeeSOAPServicesImpl class
url (wadl): http://localhost:8090/avenue-test/application.wadl

SOAP Services: in br.com.avenue.rest.services.EmployeeRestServices class
wsdl: http://localhost:8091/employeeSOAPServices?wsdl

- I not used Spring framework because i not found this tecnology in specification of this job oportunity, i really like to use Spring.
- In persistence tier a user hibernate, and h2 like a in memory data base.

- Improves to the next release: Tree of Exceptions, application log and junits.