package br.com.avenue.dao;

import org.hibernate.Session;

import br.com.avenue.util.HibernateUtil;

/**
 * 
 * @author Christian Oscar Tejada Pacheco
 *
 */
public class GenericDAO {

	private static Session session = null;

	public Session getSession() {
		if (session == null){
			session = new HibernateUtil().getSessionFactory().openSession();
			new HibernateUtil().runStarterScript();
		}
		
		return session;
	}
}
