package br.com.avenue.dao;

import java.util.List;

import br.com.avenue.entity.Employee;
/**
 * 
 * @author Christian Oscar Tejada Pacheco
 *
 */
public interface EmployeeDAO {

	/**
	 * Find a employee by id. If id is not passed by param the search will be executed without parameters.
	 * @param id
	 * @return a list of Employees
	 */
	public List<Employee> find(final Integer id);

	/**
	 * Create an Employee
	 * @param employee
	 */
	public void create(final Employee employee);
	
}