package br.com.avenue.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;

import br.com.avenue.entity.Employee;

/**
 * 
 * @author Christian Oscar Tejada Pacheco
 *
 */
public class EmployeeDAOImpl extends GenericDAO implements EmployeeDAO {

	public List<Employee> find(final Integer id) {

		Criteria  criteria = getSession().createCriteria(Employee.class);
		if (id != null) {
			criteria.add(Expression.ge("id", id));
		}
		List<Employee> employees = (List<Employee>)criteria.list();
		return employees;
	}

	public void create(final Employee employee) {

		getSession().beginTransaction();
		getSession().persist(employee);
		getSession().close();
	}
	
	
}
