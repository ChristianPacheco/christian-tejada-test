package br.com.avenue.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;

import org.h2.jdbcx.JdbcConnectionPool;
import org.h2.tools.RunScript;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 * 
 * @author Christian Oscar Tejada Pacheco
 *
 */
public class HibernateUtil {

	private static final SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			return new AnnotationConfiguration().configure().buildSessionFactory();

		}
		catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void shutdown() {
		// Close caches and connection pools
		getSessionFactory().close();
	}

	/**
	 * Run a structure and dump database script
	 */
	public void runStarterScript(){
		try {
			JdbcConnectionPool cp = JdbcConnectionPool.create(
					"jdbc:h2:~/test", "sa", "sa");
			Connection conn;
			conn = cp.getConnection();
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(this.getClass().getResourceAsStream("/create.sql")));
			RunScript.execute(conn, bufferedReader);
			conn.close();
			cp.dispose();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
