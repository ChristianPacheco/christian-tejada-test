package br.com.avenue.util;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import br.com.avenue.entity.Employee;
import br.com.avenue.model.EmployeeModel;

/**
 * 
 * @author Christian Oscar Tejada Pacheco
 *
 */
public class EmployeeUtil {

	public void copyEmployees(List<Employee> source , List<EmployeeModel> destiny) 
			throws IllegalAccessException, InvocationTargetException {
		for (Employee employeeSoure :source) {
			EmployeeModel employeeModel = new EmployeeModel();
			BeanUtils.copyProperties(employeeModel, employeeSoure);
			destiny.add(employeeModel);
		}
	}

}
