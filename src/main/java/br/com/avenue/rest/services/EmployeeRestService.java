package br.com.avenue.rest.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import br.com.avenue.model.EmployeeModel;

/**
 * 
 * @author Christian Oscar Tejada Pacheco
 *
 */
public interface EmployeeRestService {

	/**
	 * Method that returns all employees in the data base
	 * @return a list of all employees
	 */
	List<EmployeeModel> findAll();

	/**
	 * Create employee into the database
	 * @param employeeModel
	 * @return void
	 */
	String create(EmployeeModel employeeModel);

}