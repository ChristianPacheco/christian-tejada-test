package br.com.avenue.rest.services;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.avenue.dao.EmployeeDAO;
import br.com.avenue.dao.EmployeeDAOImpl;
import br.com.avenue.entity.Employee;
import br.com.avenue.model.EmployeeModel;
import br.com.avenue.util.EmployeeUtil;

/**
 * @author Christian Oscar Tejada Pacheco
 */
@Path("employeeServices")
public class EmployeeRestServicesImpl implements EmployeeRestService {

	private String msgCreateOK = "Employee Inserted sucessfully.";
	
	 Logger logger = LoggerFactory.getLogger(EmployeeRestServicesImpl.class);
	
	private EmployeeDAO employeeDAO = new EmployeeDAOImpl(); 
	
	/* (non-Javadoc)
	 * @see br.com.avenue.rest.services.EmployeeRestService1#findAll()
	 */
	@GET 
	@Path("/findAll")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EmployeeModel> findAll(){
		logger.debug("Retrieveing all employees");
		List<EmployeeModel> destiny = new ArrayList<EmployeeModel>();
		List<Employee> source = employeeDAO.find(null);
		try {
			new EmployeeUtil().copyEmployees(source, destiny);
		} catch (IllegalAccessException e) {
			logger.error("Error trying to copy properties in rest services (findAll)", e);
		} catch (InvocationTargetException e) {
			logger.error("Error trying to copy properties in rest services (findAll)", e);
		}
		return destiny;

	}

	/* (non-Javadoc)
	 * @see br.com.avenue.rest.services.EmployeeRestService1#create(br.com.avenue.model.EmployeeModel)
	 */
	@GET
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String create(@QueryParam("employee") EmployeeModel employeeModel){
		logger.debug("Creating employee name: " + employeeModel.getFirstName() + " " + employeeModel.getLastName());
		Employee employee = new Employee();
		try {
			BeanUtils.copyProperties(employee, employeeModel);
		} catch (IllegalAccessException e) {
			logger.error("Error trying to copy properties in rest services (create)", e);
		} catch (InvocationTargetException e) {
			logger.error("Error trying to copy properties in rest services (create)", e);
		}
		employeeDAO.create(employee);
		return msgCreateOK;

	}
}



