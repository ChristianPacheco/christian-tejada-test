package br.com.avenue.exception;

/**
 * 
 * @author Christian Oscar Tejada Pacheco
 *
 */
public class BusinessException extends Exception {
	
	public BusinessException(String  message){
		super(message);
	}

}
