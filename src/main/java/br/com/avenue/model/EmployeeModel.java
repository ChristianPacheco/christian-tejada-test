package br.com.avenue.model;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Christian Oscar Tejada Pacheco
 *
 */
@XmlRootElement
public class EmployeeModel {
	
	private int id;
	
	private String firstName;
	
	private String lastName;
	
	private BigDecimal salary;

	public EmployeeModel(){
		
	}
	
	public EmployeeModel(int id, String firstName, String lastName, BigDecimal salary) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.salary = salary;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

}
