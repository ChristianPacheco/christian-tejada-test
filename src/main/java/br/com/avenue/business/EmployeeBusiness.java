package br.com.avenue.business;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import br.com.avenue.entity.Employee;

/**
 * @author Christian Oscar Tejada Pacheco
 *
 */
public class EmployeeBusiness {

	/**
	 * Calculate de avarage of a list of salaries.
	 * @param employees
	 * @return avarage of salaries
	 */
	public BigDecimal calculateAvarageSalary(final List<Employee> employees) {
		BigDecimal totalSalary = BigDecimal.ZERO;
		for (Employee employee : employees) {
			totalSalary = totalSalary.add(employee.getSalary());
		}
		return totalSalary.divide(BigDecimal.valueOf(employees.size()));
	}
	
	public BigDecimal getRanbomBonus(final BigDecimal salary) {
		BigDecimal ranbomValue = BigDecimal.ZERO;
		while (ranbomValue.compareTo(BigDecimal.ZERO) == 0){
			double ranbom = Math.random();
			if (ranbom < 0.5){
				ranbomValue = BigDecimal.valueOf(ranbom);
				break;
			}
		}
		return salary.multiply(ranbomValue).setScale(2, RoundingMode.HALF_EVEN);
	}
	
}
