package br.com.avenue.soap.services;


import java.math.BigDecimal;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.avenue.business.EmployeeBusiness;
import br.com.avenue.dao.EmployeeDAO;
import br.com.avenue.dao.EmployeeDAOImpl;
import br.com.avenue.entity.Employee;

/**
 * @author Christian Oscar Tejada Pacheco
 *
 */
@WebService(endpointInterface = "br.com.avenue.soap.services.EmployeeSOAPServices",
serviceName = "EmployeeSOAPServices")
public class EmployeeSOAPServicesImpl implements EmployeeSOAPServices {

	Logger logger = LoggerFactory.getLogger(EmployeeSOAPServicesImpl.class);
	
	private EmployeeDAO employeeDAO = new EmployeeDAOImpl(); 
	
	/* (non-Javadoc)
	 * @see br.com.avenue.soap.services.IEmployeeSOAPServices#getAverageEmployeesSalary()
	 */
	@WebMethod
	public BigDecimal getAverageEmployeesSalary() {
		logger.debug("Calculating the avarage of all salaries");
		List<Employee> employees = employeeDAO.find(null);
		return new EmployeeBusiness().calculateAvarageSalary(employees);
	}
	
	/* (non-Javadoc)
	 * @see br.com.avenue.soap.services.IEmployeeSOAPServices#calculateEmployeeBonus(java.lang.Long)
	 */
	@WebMethod
	public BigDecimal calculateEmployeeBonus(final Integer employeeId){
		logger.debug("Calculating bonus to employee id: " + employeeId);
		List<Employee> employees = employeeDAO.find(employeeId);
		if (employees !=null && !employees.isEmpty()) {
			return new EmployeeBusiness().getRanbomBonus(employees.get(0).getSalary());
		}
		return null;
	}
	
	
}
