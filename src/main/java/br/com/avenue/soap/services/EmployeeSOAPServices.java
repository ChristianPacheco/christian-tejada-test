package br.com.avenue.soap.services;

import java.math.BigDecimal;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * 
 * @author Christian Oscar Tejada Pacheco
 *
 */
@WebService
public interface EmployeeSOAPServices {

	/**
	 * 
	 * @return avarage salary of all employees
	 */
	BigDecimal getAverageEmployeesSalary();

	/**
	 * Return the bonus per employee
	 * @param employeeId
	 * @return bonus of specific employee
	 */
	BigDecimal calculateEmployeeBonus(@WebParam(name="employeeId")final Integer employeeId);

}