CREATE TABLE IF NOT EXISTS employees (
	id INT AUTO_INCREMENT PRIMARY KEY,
	first_name VARCHAR NOT NULL,
	last_name VARCHAR NOT NULL,
	salary DECIMAL(8,2) NOT NULL
);
delete from employees;
commit;
INSERT INTO employees (first_name, last_name, salary) VALUES ('John', 'Doe', 5000);
INSERT INTO employees (first_name, last_name, salary) VALUES ('Christian', 'Tejada', 2000);
commit;