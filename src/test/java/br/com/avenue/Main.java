package br.com.avenue;

import java.io.IOException;
import java.net.URI;

import javax.xml.ws.Endpoint;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.avenue.rest.services.EmployeeRestServicesImpl;
import br.com.avenue.soap.services.EmployeeSOAPServices;
import br.com.avenue.soap.services.EmployeeSOAPServicesImpl;

/**
 * 
 * @author Christian Oscar Tejada Pacheco
 * Main test
 */
public class Main {
	
    public static final String BASE_URI = "http://localhost:8090/avenue-test/";
    public static final String RESOURCE_PACKAGE = "br.com.avenue.rest.services";
    public static final String SOAP_ADDRESS = "http://localhost:8091/employeeSOAPServices";
    
    static Logger logger = LoggerFactory.getLogger(EmployeeRestServicesImpl.class);
    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    @Test
    private static HttpServer startRestServer() {
    	logger.info("________________Starting REST Server____________");
    	
    	final ResourceConfig rc = new ResourceConfig().packages(RESOURCE_PACKAGE);
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    @Test
    private static void startSOAPServer() {
    	logger.info("________________Starting SOAP Server____________");
    	EmployeeSOAPServices implementor = new EmployeeSOAPServicesImpl();
    	
    	Endpoint.publish(SOAP_ADDRESS, implementor);
    }
    
    /**
     * Main method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        final HttpServer server = startRestServer();
        logger.info(String.format("______________Jersey app started with WADL available at "
                + "%sapplication.wadl____________", BASE_URI));
        
        startSOAPServer();
        
        logger.info(String.format("SOAP app started with WSDL available at " + SOAP_ADDRESS+"?wsdl"));     
    }
}

